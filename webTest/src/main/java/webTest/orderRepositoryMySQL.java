package webTest;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class orderRepositoryMySQL {
	private Connection conn;
	
	public orderRepositoryMySQL(Connection conn) {
		this.conn = conn;
	}
	
	public void insertOrder(String createdAt) throws SQLException {
		
			String sql = "INSERT INTO order_id(createdAt)";
			sql = sql + "VALUES(?)";
			
			PreparedStatement pstm = conn.prepareStatement(sql);
			pstm.setString(1, createdAt);
			
			int result = pstm.executeUpdate();
			
			if(result > 0) {
				System.out.println("insert data order sukses");
			}else {
				System.out.println("insert data gagal");
			}
			
			if(result > 0) {
				conn.commit();
			}else {
				conn.rollback();
			}
			
			pstm.close();
			//conn.close();
			
	}
}
