package webTest;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class orderItemRepositoryMySQL {
	private Connection conn;
	
	public orderItemRepositoryMySQL(Connection conn) {
		this.conn = conn;
	}

	public void insertOrderItem(int id, String code, String name, String type, int price, int quantity) throws SQLException {
		// TODO Auto-generated method stub
			
			String sql2 = "INSERT INTO order_item(id, code, name, type, price, quantity)";
			sql2 = sql2 + "VALUES(?,?,?,?,?,?)";
			
			PreparedStatement pstm2 = conn.prepareStatement(sql2);
			pstm2.setInt(1, id);
			pstm2.setString(2, code);
			pstm2.setString(3, name);
			pstm2.setString(4, type);
			pstm2.setInt(5, price);
			pstm2.setInt(6, quantity);
			
			int result2 = pstm2.executeUpdate();
			
			if(result2 > 0) {
				System.out.println("insert data order item sukses");
			}else {
				System.out.println("insert data gagal");
			}
			
			if(result2 > 0) {
				conn.commit();
			}else {
				conn.rollback();
			}
			
			pstm2.close();
			//conn.close();
	}
}
	


