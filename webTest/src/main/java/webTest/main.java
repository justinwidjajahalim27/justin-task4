package webTest;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.jasper.tagplugins.jstl.core.Out;

/**
 * Servlet implementation class main
 */
@WebServlet("/main")
public class main extends HttpServlet {
	private Connection conn;
	
	public main(Connection conn) {
		this.conn = conn;
	}
	
	private loginValidation loginValidation;
	
	public void init() {
		loginValidation = new loginValidation(conn);
	}
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public main() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String jdbcURL = "jdbc:mysql://localhost:3306/assignment";
        String dbUser = "root";
        String dbPassword = "";
        
        
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection connection = DriverManager.getConnection(jdbcURL, dbUser, dbPassword);
			
			String sqlx1 = "SELECT order_id.id, order_item.code, order_item.name, order_item.type, order_item.price, order_item.quantity";
			sqlx1 = sqlx1 + "FROM order_item";
			sqlx1 = sqlx1 + "JOIN order_id ON order_item.id=order_id.id";
			Statement pstmx = conn.createStatement();
			ResultSet rs = pstmx.executeQuery(sqlx1);
			PrintWriter out1 = response.getWriter();
			out1.println("<table border=1 width=75%>");
			while(rs.next()) {
				out1.println("<tr>");
				out1.println(rs.getInt("id" +"</td>"));
				out1.println(rs.getString("code" +"</td>"));
				out1.println(rs.getString("name" +"</td>"));
				out1.println(rs.getInt("price" +"</td>"));
				out1.println(rs.getInt("quantity" +"</td>"));
				out1.println("</tr>");
			}
			out1.println("</table>");
			pstmx.close();

		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String jdbcURL = "jdbc:mysql://localhost:3306/assignment";
        String dbUser = "root";
        String dbPassword = "";
        
        try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection connection = DriverManager.getConnection(jdbcURL, dbUser, dbPassword);
	        
			PrintWriter out = response.getWriter();
			String username = request.getParameter("username");
			String password = request.getParameter("password");
			User user = new User();
			user.setUsername(username);
			user.setPassword(password);
			
			if (loginValidation.Validation(user)) {
				out.println("Welcome " + user.getUsername());
			} else {
				out.println("SALAH !");
			}
			
		} catch (ClassNotFoundException | SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        
		
			

		
		
	}

}
