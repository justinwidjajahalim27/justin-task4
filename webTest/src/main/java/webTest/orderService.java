package webTest;

import java.sql.Connection;
import java.sql.SQLException;

public class orderService {
	private Connection conn;
	
	public orderService(Connection conn) {
		this.conn = conn;
	}
	
	public void saveOrder() {
		try {
			conn.setAutoCommit(false);
			
			new orderRepositoryMySQL(conn).insertOrder("2021-01-20");
			new orderRepositoryMySQL(conn).insertOrder("2021-01-20");
			new orderItemRepositoryMySQL(conn).insertOrderItem(1, "123", "Kue Basah", "Makanan", 5000, 5);
			new orderItemRepositoryMySQL(conn).insertOrderItem(2, "321", "Kue Mangkok", "Makanan", 2000, 5);
			
			
			conn.commit();
			
		} catch (SQLException e) {
			if(conn != null) {
				try {
					conn.rollback();
					conn.close();
				} catch (SQLException ee) {
					ee.printStackTrace();
				}
			}
		}
		
	}

		
	}


